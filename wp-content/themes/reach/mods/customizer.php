<?php

function wpb_customize_register($wp_customize){
/*
	//showcase
	$wp_customize->add_section('extendidentity', array(
		'title'=>__('Extended Site Identity', 'reach'),
		'description'=>sprintf(__('Extended Site Identity', 'reach')),
		'priority'=>130
	));

	//SITE NAME
	$wp_customize->add_setting('extendidentity_number', array(
		'default'=>_x('01942100100', 'reach'),
		'type'=>'theme_mod'
	));
	$wp_customize->add_control('extendidentity_number', array(
		'label'=>__('Default phone number', 'reach'),
		'section'=>'extendidentity',
		'priority'=>1
	));

	//SITE PLACE
	$wp_customize->add_setting('extendidentity_place', array(
		'default'=>_x('Wigan', 'reach'),
		'type'=>'theme_mod'
	));
	$wp_customize->add_control('extendidentity_place', array(
		'label'=>__('Default location', 'reach'),
		'section'=>'extendidentity',
		'priority'=>2
	));

	//SITE USPS
	$wp_customize->add_setting('extendidentity_usps', array(
		'default'=>_x('One fixed Price|Next day Installation|NIC EIS Approved|Gas Safe Approved|', 'reach'),
		'type'=>'theme_mod'
	));
	$wp_customize->add_control('extendidentity_usps', array(
		'label'=>__('All USPs', 'reach'),
		'section'=>'extendidentity',
		'type'=>'textarea',
		'priority'=>3
	));

	//LOGO
	$wp_customize->add_setting('extendidentity_logo', array(
		'default'=>get_bloginfo('template_directory').'/rsc/logo.png',
		'type'=>'theme_mod'
	));
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'extendidentity_logo',array(
		'label'=>__('Logo (PAGE TOP)', 'reach'),
		'section'=>'extendidentity',
		'settings'=>'extendidentity_logo',
		'priority'=>4
	)));

	//LOGO MOB
	$wp_customize->add_setting('extendidentity_logom', array(
		'default'=>get_bloginfo('template_directory').'/rsc/logo-sm.png',
		'type'=>'theme_mod'
	));
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'extendidentity_logom',array(
		'label'=>__('Logo MOBILE (PAGE TOP)', 'reach'),
		'section'=>'extendidentity',
		'settings'=>'extendidentity_logom',
		'priority'=>5
	)));

	//LOGO (footer)
	$wp_customize->add_setting('extendidentity_logof', array(
		'default'=>get_bloginfo('template_directory').'/rsc/logoW.png',
		'type'=>'theme_mod'
	));
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'extendidentity_logof',array(
		'label'=>__('Logo (PAGE FOOTER)', 'reach'),
		'section'=>'extendidentity',
		'settings'=>'extendidentity_logof',
		'priority'=>6
	)));

	//LOGO MOB (footer)
	$wp_customize->add_setting('extendidentity_logofm', array(
		'default'=>get_bloginfo('template_directory').'/rsc/logoW-sm.png',
		'type'=>'theme_mod'
	));
	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize,'extendidentity_logofm',array(
		'label'=>__('Logo MOBILE (PAGE FOOTER)', 'reach'),
		'section'=>'extendidentity',
		'settings'=>'extendidentity_logofm',
		'priority'=>7
	)));



	//SITE AREAS
	$wp_customize->add_setting('extendidentity_areas', array(
		'default'=>_x("Altrincham|01617144409\nBamber Bridge|01772591443\nBirkenhead|01515394322\nBlackburn|01254443437\nBlackpool|01253363436\nBolton|01204262464\nBootle|01515394342\nBurnely|01282908448\nBury|01617144442\nChester|01244307443\nChorley|01257544449\nCrewe|01270337473\nElllesmere Port|01515394314\nFrodsham|01928264428\nHale|01617144274\nHaydock|01744324406\nKnutsford|01565357408\nLeigh|01942388493\nLeyland|01772591428\nLiverpool|01515394265\nLowton|01942388433\nMacclesfield|01625313435\nManchester|01617144364\nNewton le willow|01925388465\nNorthwich|01606226462\nOrmskirk|01695373435\nPreston|01772591404\nRochdale|01706484489\nRuncorn|01928264486\nSale|01617144347\nSandbach|01270337400\nSkelmersdale|01695373456\nSouthport|01704489488\nSt Helens|01744324412\nSwinton|01617144428\nWarrington|01925388432\nWidnes|01515394196\nWigan|01942388484\nWinsford|01606226449", 'reach'),
		'type'=>'theme_mod'
	));
	$wp_customize->add_control('extendidentity_areas', array(
		'label'=>__('All Areas Covered', 'reach'),
		'section'=>'extendidentity',
		'type'=>'textarea',
		'priority'=>8
	));


	//SITE SERVICES
	$wp_customize->add_setting('extendidentity_services', array(
		'default'=>_x("General Electrical\nFusebox Install\nElectrical Test\nRewire\nGeneral Gas\nBoiler Install\nBoiler Service\nGas Safety Check\nLandlords Gas Safety Check", 'reach'),
		'type'=>'theme_mod'
	));
	$wp_customize->add_control('extendidentity_services', array(
		'label'=>__('All Services Done', 'reach'),
		'section'=>'extendidentity',
		'type'=>'textarea',
		'priority'=>9
	));
*/
}

add_action('customize_register','wpb_customize_register');