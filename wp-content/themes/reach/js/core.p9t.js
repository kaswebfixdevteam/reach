
$(document).ready(function(){

//stick menu to top of screen
$(window).scroll(function(e){
  var h= $(this).scrollTop();
  var navis= $("header");
  if(h > 60){
    navis.addClass("stickit");
  }else{
    navis.removeClass("stickit");
  }
});

// Select all links with hashes
	$('#navbarNavDropdown .navbar-nav.ml-auto a,a.jumpit').click(function(e){
		e.preventDefault();
		jumptoarea(this);
	});
	$("#jumpup").click(function(){
		$('#navbarNavDropdown .navbar-nav.ml-auto li').removeClass("active");
		$("html, body").animate({ scrollTop: "0px" });
		$('#navbarNavDropdown .navbar-nav.ml-auto li#top').addClass("active");
	});
});

function jumptoarea(jumptoY){
	if($(jumptoY).parent()[0].localName=="li"){
		$('#navbarNavDropdown .navbar-nav.ml-auto li').removeClass("active");
		$(jumptoY).parent().addClass("active");
	}
	var jumptolnk= parseInt($(jumptoY.hash)[0].offsetTop)-86;
	$("html, body").animate({ scrollTop: jumptolnk+"px" });
	if(jumptoY.className=="nav-link"){
		if ($(window).width() < 768) {
		    $("button.navbar-toggler").click();
		}
	}
}