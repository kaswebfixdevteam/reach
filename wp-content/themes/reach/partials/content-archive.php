<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="post-header">
    <?php 
    if(has_post_thumbnail()) : 
      the_post_thumbnail();
    endif;
    ?>
     <div class="date"><?php the_time( 'F j, Y g:i a' ); ?></div>
     <h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
     <div class="author">
      <?php echo get_author_posts_url(get_the_author_meta('ID')); ?> link to author posts
      <?php the_author(); ?>
     </div>
  </div><!--end post header-->
  <div class="entry clear">
     <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail(); ?>
     <?php the_excerpt(); ?>
     <?php edit_post_link(); ?>
     <?php wp_link_pages(); ?> </div>
  <!--end entry-->
  <div class="post-footer">
     <div class="comments"><?php comments_popup_link( 'Leave a Comment', '1 Comment', '% Comments' ); ?></div>
  </div><!--end post footer-->
</div><!--end post-->