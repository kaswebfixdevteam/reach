<?php get_header(); ?>
    <main role="main">
      <i class="fab fa-apple-pay"></i>
      <?php if ( have_posts() ) : ?>


<?php while ( have_posts() ) : the_post(); ?>
    <?php get_template_part( 'partials/content', get_post_format() ); ?>
<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>

    <div class="navigation index">
       <div class="alignleft"><?php next_posts_link( 'Older Entries' ); ?></div>
       <div class="alignright"><?php previous_posts_link( 'Newer Entries' ); ?></div>
    </div><!--end navigation-->


<?php else : ?>

    <p><?php __('No Post Found');?></p>

      <?php endif; ?>
      <?php //get_sidebar(); ?>
    </main>
<?php get_footer(); ?>