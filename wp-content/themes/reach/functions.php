<?php
// Register Nav Walker class_alias
require_once('mods/class-wp-bootstrap-navwalker.php');

//Theme Support
function wpb_theme_setup(){

    add_theme_support('post-thumbnails');
    
    register_nav_menus(array(
        'primary'=> __('Primary Menu')
    ));
    //Post Formats
    add_theme_support('post-formats', array('aside','gallery'));
}

add_action('after_setup_theme','wpb_theme_setup');


//Excerpt length control
function set_excerpt_length(){
    return 50;
}
add_filter('excerpt_length', 'set_excerpt_length');


//widget location
function wpb_init_widgets($id){
	register_sidebar(array(
	'name' => 'Sidebar',
	'id' => 'sidebar',
	'before_widget' => '<div class="sidebar-module">',
	'after_widget' => '</div>',
	'before_title' => '<h4>',
	'after_title' => '</h4>'
	));
	/*
	register_sidebar(array(
	'name' => 'Box1',
	'id' => 'box1',
	'before_widget' => '<div class="box">',
	'after_widget' => '</div>',
	'before_title' => '<h3>',
	'after_title' => '</h3>'
	));
	*/
}


add_action('widgets_init', 'wpb_init_widgets');
require( get_template_directory().'/mods/customizer.php');


?>